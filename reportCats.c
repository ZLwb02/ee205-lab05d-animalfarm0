//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file reportCats.c
//### @version 1.0
//###
//### Report Cats Module - Finds cats from database and prints database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//###############################################################################
#include "reportCats.h"
#include "catDatabase.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

extern size_t currentCatNum; // Declared externally in catDatabase.c

//Prints the data stored in the array of a specific cat given an index
int printCat(long unsigned index){
    if(index < 0 || index > currentCatNum){
        printf("animalFarm0: Bad cat %ld",index);
        //exit(EXIT_FAILURE);
        return -1; 
    }
    else{
        printf("cat index= %lu ",index);
        printf("name= %s ",catdb[index].name);
        printf("gender= %d ",catdb[index].gender);
        printf("breed= %d ",catdb[index].breed);
        printf("isFixed= %d ",catdb[index].isFixed);
        printf("weight= %f\n",catdb[index].weight);
        return 0;
    }
}

int printAllCats(){
    for(int i = 0; i < currentCatNum; i++){
        printCat(i);
    }
    return 0;
}

int foundindex;
int findCat( char lookupName[] ){
    bool found = false;
    for( int i = 0; i < currentCatNum; i++ ){
        if( strcmp(catdb[i].name, lookupName ) == 0){
            found = true;
            foundindex = i;
        }   
    }

    if( found == true ){
        //printf("Found da cat!\n");
        return foundindex;
    }
    else{
        printf("Cat not found.\n");
        //exit(EXIT_FAILURE);
        return -1;
    }
}
        
