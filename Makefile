###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05d - animalFarm0 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build and test a program that modifys a cat database
###
### @author  Zack Lown <zlown@hawaii.edu>
### @date    16 Feb 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

# Build a program for Creating and modifying a cat database

HOSTNAME=$(shell hostname --short)
$(info $(HOSTNAME))

CC     = gcc
CFLAGS = -g -Wall -DHOST=\"$(HOSTNAME)\"

TARGET = animalFarm0

all: clearscreen $(TARGET)

.PHONY: clearscreen

clearscreen:
	clear

# For now, do not use Make's % expansion parameters.  Each file must have its own 
# build instructions.  We will use the % expansion later.

##integers.o: integers.c integers.h         -- rule example
##		$(CC) $(CFLAGS) -c integers.c

main.o: main.c config.h
	$(CC) $(CFLAGS) -c  main.c

addCats.o: addCats.c addCats.h
	$(CC) $(CFLAGS) -c addCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

animalFarm0:main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET)  main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o

test: clearscreen $(TARGET)
	./$(TARGET)

clean:
	rm -f *.o $(TARGET)

   ##$(CC) $(CFLAGS) -o $(TARGET) addCats.o
   ##$(CC) $(CFLAGS) -o $(TARGET) deleteCats.o
   ##$(CC) $(CFLAGS) -o $(TARGET) reportCats.o
   ##$(CC) $(CFLAGS) -o $(TARGET) updateCats.o

