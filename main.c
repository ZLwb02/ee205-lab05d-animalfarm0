//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file .gitignore
//### @version 1.0
//###
//### Main program used for running AnimalFarm
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//###############################################################################
#include "addCats.h"
#include "catDatabase.h"
#include <stdbool.h>    //For Boolean operator
#include <stdio.h>      // For strings etc

int main(int argc, char* argv[]){

    if(DEBUGMODE){
    //-----------------------------------------Check addCats & db-------------------------------------------
        printf("\n----addCats & db checking----");
        addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
        addCat( "Milo", MALE, MANX, true, 7.0 ) ;
        addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
        addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
        addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
        addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
        //Purposeful error checking
        printf("\n-There should be 4 Different errors below:\n");
        addCat("", MALE, PERSIAN, true, 6.5); //Name string cannot be empty        
        addCat("HappyBirthdayToYou,HappyBirthdayDearProfessorNelson",MALE ,UNKNOWN_BREED, false, 100.8 ); //Name must be < 30 chars
        addCat( "Loki", MALE, PERSIAN, true, 8.5 ); //Cats can't have same name
        addCat( "Mauritus", MALE, PERSIAN, true, (double)0 ); //Cats weight must be > 0
    
        //-------------------------------------------Check reportCats ------------------------------------------------
        printf("\n----reportCats checking----\n");
        printCat(0);// Prints Loki
        printCat(3); //Print Kali
        printf("Index of Cat Chili: %d\n",findCat("Chili"));//Prints 5
        printf("Index of Cat Bella: %d\n",findCat("Bella"));//Prints 2
        printf("-All the Cats in the database\n");
        printAllCats();
        //purposeful error checking
        printf("-There should be 1 error below :\n");
        findCat("hemrid");//Errors because cat is not in db
    
        //----------------------------------------Check updateCat-----------------------------------------------------
        printf("\n----updateCats checking----\n");
        //Next two lines do purposeful error checking
        int kali = findCat( "Kali" ) ;
        updateCatName( kali, "Chili" ) ; // this should fail
        printCat( kali );
        updateCatName( kali, "Capulet" ) ;
        updateCatWeight( kali, 9.9 ) ;
        fixCat( kali ) ;
        printCat( kali );

        //--------------------------------------deleteCats-----------------------------------------------------------
        printf("\n----deleteCats checking----\n");
        printAllCats();
        deleteCat(3);
        printf("-Should have removed the 3rd cat in the array\n");
        printAllCats();//Should have removed cat #2 in the array list
        deleteAllCats();
        printf("-There should be nothing directly below this:\n");
        printAllCats();
        printf("\n");
    }//End of debug code

    //Provided main program testing
    addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
    addCat( "Milo", MALE, MANX, true, 7.0 ) ;
    addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
    addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
    addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
    addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
    printAllCats();
    int kali = findCat( "Kali" ) ;
    updateCatName( kali, "Chili" ) ; // this should fail
    printCat( kali );
    updateCatName( kali, "Capulet" ) ;
    updateCatWeight( kali, 9.9 ) ;
    fixCat( kali ) ;
    printCat( kali );
    printAllCats();
    deleteAllCats();
    printAllCats();
}//End of main()
