//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file deleteCats.c
//### @version 1.0
//###
//### Delete Cats Module - removes cats from database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//##############################################################################
#include "catDatabase.h"
#include <stdlib.h>//Defines datatype size_t
#include <stdio.h>//For printf


extern size_t currentCatNum; // Declared externally in catDatabase.c

int deleteAllCats(){
    currentCatNum = 0;
    return 0;
}

int deleteCat(int index){
    if( index > currentCatNum ){
        printf("Cannot Delete this item as it's outside of the database.");
        return -1;
    }
    else{
        for(int i = index-1; i < currentCatNum-1; i++){
            catdb[i]= catdb[i+1]; //Move the next element(i+1) one down (i) 
        }
        currentCatNum -=1;//Moves the index back one
        return 0;
    }
}
