//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file addCats.h
//### @version 1.0
//###
//### Add Cats Module header - adds cats to the database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//###############################################################################

#ifndef ADDCATS_H
#define ADDCATS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "catDatabase.h"

int addCat(char name[], genders gender, breeds breed, bool isFixed, double weight);

#endif 
