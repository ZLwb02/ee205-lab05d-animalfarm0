//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file updateCats.c
//### @version 1.0
//###
//### Update Cats Module- Updates the cats in the database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022 
//###############################################################################
#include <stdbool.h> //For bool data type
#include <stdio.h> //For printing
#include <stdlib.h> 
#include <string.h> //For strcpy
#include "catDatabase.h"
#include "config.h"

extern size_t currentCatNum; // Declared externally in catDatabase.c

int updateCatName(int index, char newName[]){
    int unique = true;
    for(int i= 0; i <= currentCatNum; i++){ //Determine if name is unique
        if( strcmp(catdb[i].name,newName) == 0 ){
            unique = false;
        }
    }
    //Check all requirements for new name
    if( strlen(newName) <= 0 ){ // If name parameter is empty...
        printf("The cat's name cannot be blank.\n");
        //exit(EXIT_FAILURE);
        return -1;        
    }
    else if( strlen(newName) >= MAX_NAME_LENGTH){ //If name parameter is too long...
        printf("The cat's name is too long; must be under 30 charachters.\n");
        //exit(EXIT_FAILURE);
        return -1;
    }
    else if (unique == false){
        //exit(EXIT_FAILURE);
        printf("This cat's name is already taken, please choose another.\n");
        return -1;
    }
    else{
        strcpy(catdb[index].name, newName);
        return 0;
    }
}//End of upDateCatName

int fixCat(int index){
    if(catdb[index].isFixed == true){
        printf("This cat has already been fixed. What did you cut off!?");
        return 0; //Not really an error so still returning 0
    }
    else{
        catdb[index].isFixed = true;
        return 0;
    }
}//End of fixCat

int updateCatWeight(int index, double newWeight){
    if(newWeight <= 0 || newWeight > 1000){ // Cat weight must be >=0 and reasonable
        printf("The cat's weight must be greater than zero.");
        return -1;
    }
    else{
    catdb[index].weight = newWeight;
    return 0;
    }
}//End of updateCatWeight
