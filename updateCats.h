//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file updateCats.h
//### @version 1.0
//###
//### Update Cats Module header- Updates the cats in the database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022 
//###############################################################################

#ifndef UPDATECATS_H
#define UPDATECATS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int updateCatName(int index, char newName[]);
int fixCat(int index);
int updateCatWeight(int index, double newWeight);

#endif 
