//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file catDatabase.h
//### @version 1.0
//###
//### Cat Database header - Defines all of the enums,the arrays, max array & num sizes, 
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//##############################################################################
#include <stdbool.h> //Defines bool data type for isFixed in struct Cat
#include "config.h" // For MAX's'

#ifndef catDatabase_h //Makes sure this file is only included once during program compilation
#define catDatabase_h

//Defines the enum's
typedef enum {UNKNOWN_GENDER,MALE,FEMALE} genders;
typedef enum {UNKNOWN_BREED,MAINE_COON,MANX,SHORTHAIR,PERSIAN,SPHYNX} breeds;

//This creates a new datatype (cats) with the applicable fields.
struct Cat {
    char name[MAX_NAME_LENGTH];
    genders gender;     //enum genders declared in catDatabase.h
    breeds breed;       //enum breeds declared in catDatabase.h
    bool isFixed;
    float weight;
};

//This creates the main array(catdb) that holds [MAX_CATS] number of cats. Values initialized to 0
struct Cat catdb[MAX_CATS]; //This does not declare a struct! Struct just says Cat is type struct.

#endif


