//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file addCats.c
//### @version 1.0
//###
//### Add Cats Module - adds cats to the database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//###############################################################################
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h> //Declares bool type
#include "catDatabase.h"

extern size_t currentCatNum; // Declared externally in catDatabase.c

//Used to add cat to the database
int addCat(char name[], genders gender, breeds breed, bool isFixed, double weight){
    int unique = true;
    for(int i= 0; i <= currentCatNum; i++){ //Determine if name is unique
        if( strcmp(catdb[i].name,name) == 0 ){
            unique = false;
        } 
    }
    if( currentCatNum >= MAX_CATS ){ // If database is already full...
        printf("The cat database if already full! Cannot add any more cats.\n");
        //exit(FAILURE);
        return -1;
    }
    else if( strlen(name) <= 0 ){ // If name parameter is empty...
        printf("The cat's name cannot be blank.\n");
        //exit(EXIT_FAILURE);
        return -1;        
    }
    else if( strlen(name) >= MAX_NAME_LENGTH){ //If name parameter is too long...
        printf("The cat's name is too long; must be under 30 charachters.\n");
        //exit(EXIT_FAILURE);
        return -1;
    }
    else if( unique == false){ // If name is not unique
        printf("This cat's name is already taken, please choose another.\n");
        //exit(EXIT_FAILURE);
        return -1;
    }
    else if( weight <= 0 || weight > 1000 ){ //If weight is not greater than zero or unreasonable
        printf("The cat's weight must be greater than zero and reasonable.\n");
        //exit(EXIT_FAILURE);
        return -1;
    }
    else{
        strcpy(catdb[currentCatNum].name, name);
        catdb[currentCatNum].gender = gender;
        catdb[currentCatNum].breed = breed;
        catdb[currentCatNum].isFixed = isFixed;
        catdb[currentCatNum].weight = weight;
        currentCatNum += 1;
        return currentCatNum-1;// -1 to get last cat in db, current cat num holds next cat index avalible 
    }
}
