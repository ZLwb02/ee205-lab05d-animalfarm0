//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file deleteCats.h
//### @version 1.0
//###
//### Delete Cats Module header- removes cats from database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//##############################################################################
#ifndef DELETECATS_H
#define DELETECATS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int deleteAllCats();
int deleteCat(int index);

#endif 
