//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file catDatabase
//### @version 1.0
//###
//### Cat Database - Defines all of the enums,the arrays, max array & num sizes, 
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//##############################################################################

#include "catDatabase.h"
#include <string.h>     //Define strings
#include <stdbool.h>    //Defines the boolean datatype
#include <stddef.h>     //Defines the size_t datatype

//This keeps track of the current number of cats. Use externally with "extern size_t currentCatNum;"
size_t currentCatNum = 0;



    

