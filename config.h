//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file config.h
//### @version 1.0
//###
//### Header file for the program
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//###############################################################################

#ifndef config_h //Makes sure this file is only included once during program compilation
#define config_h


#define MAX_NAME_LENGTH     30
#define MAX_CATS            50  //Maximum Cats allowed to be in catDatabase array
#define DEBUGMODE           1   //0 for Debug mode off, 1 for on

#endif
