//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 05d -AnimalFarm0 - EE 205 - Spr 2022
//###
//### @file reportCats.h
//### @version 1.0
//###
//### Report Cats Module header- Finds cats from database and prints database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   17 Feb 2022
//###############################################################################

#ifndef REPORTCATS_H
#define REPORTCATS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "catDatabase.h"

int printCat(long unsigned index);
int printAllCats();
int findCat(char lookupName[]);

#endif 
